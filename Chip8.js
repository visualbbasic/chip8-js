var memorySize = 4096;
var memory;

//V registers
//15 6-BIT general purpose registers named V0,V1,VE 16th is 'carry flag'
//note - have fun
var vRegisterSize = 16;
var V;

//Index register (0x000 ~ 0xFFF)
// REGISTER_I -> address register, used with several opcodes that involve memory operations
// pc -> program counter
var REGISTER_I;
var pc;

//stack is used to remember current location before
//jump is performed. Anytime jump or calling a subroutine is performed,
//store the program counter in stack.
var stackSize = 16;
var stack;
var sp; //stack pointer

//Intended to be used for timing the events of games. Its value can be set and read.
var delay_timer;
//Timer used for sound effects. When its value is nonzero, a beeping sound is made.
var sound_timer;

//implement this in input
//16-pad keys
var key = new Array(16);


var font_set =
[
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
];


function init(program) {
    pc = 0x200;
    opcode = 0;
    REGISTER_I = 0;
    sp = 0;
    console.log("inited I: " + REGISTER_I);
    //clear display

    clearStack();
    clearMemory();
    clearVRegister();

    // LOADING FONT SET
    //0 ~ whatever
    loadFontSet();
    // LOADING PROGRAM
    // 0x200 to 0xFFF
    loadProgram(program);

    //Initiate graphics
    initGraphics();


}

function clearStack() {
    stack = new Array(stackSize);
}

function clearMemory() {
    memory = new Array(memorySize);
}

function clearVRegister() {
    V = new Array(vRegisterSize);
}

function loadFontSet() {
    for(var i = 0;  i < font_set.length; i++){
        memory[i] = font_set[i];
    }
}

function loadFonts() {

}

function loadProgram(program) {
    var start = 512;
    var count = 0;
    var programLength = program.length;
    for (var i = start; i < memorySize; i++) {
        if (count < programLength) {
            memory[i] = program[count];
            count++;
        }
    }
}