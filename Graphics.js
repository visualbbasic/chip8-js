/**
 * Created with JetBrains WebStorm.
 * User: JasonOwnzBiatch
 * Date: 22/06/13
 * Time: 4:20 PM
 * To change this template use File | Settings | File Templates.
 */

    //note these two must be opposite
var GFX_WIDTH = 64;
var GFX_HEIGHT = 32;
var CANVAS_WIDTH = GFX_WIDTH * 10;
var CANVAS_HEIGHT = GFX_HEIGHT * 10;
//implement this in graphics
var gfx;

function initCanvas(id){
    var canvas = document.getElementById(id);
    canvas.style.width = CANVAS_WIDTH + "px";
    canvas.style.height = CANVAS_HEIGHT + "px";
    canvas.style.border = "1px solid black";
}

function initGraphics(){
    gfx = new Array(GFX_WIDTH);
    for(var i = 0; i < gfx.length; i++){
        gfx[i] = new Array(GFX_HEIGHT);
        for(var j = 0; j < gfx[i].length; j++){
            gfx[i][j] = 0;
        }
    }
}

function clearScreen(){

}

function draw(x,y,w,h){
    //assigning values to double array GFX
    var pixel;
    console.log("x: " + x + " y: " + y + " w: " + w + "  h: " + h + " REGISTER_I: " + REGISTER_I);
    //Resetting register VF
    V[0xF] = 0;
    for(var row = 0; row < h; row++){
        pixel = memory[REGISTER_I + row];
        console.log("pixel: " + pixel);
        for(var col = 0; col < w; col++){
            console.log("x+col " + (x+col) + "  " + (y+row));
            var target =   gfx[(x + col)][(y + row)];
            //Check if the pixel on the display is set to 1. If it is set,
            //we need to register the collision by setting the VF register.

            if((pixel & (0x80 >> col)) != 0){
                //Check if the pixel on the display is set to 1.
                // If it is set we need to register the collision
                // by setting the VF register
                if(target == 1)
                    V[0xF] = 1;


                // setting value using XOR
                gfx[(x + col)][(y + row)] ^= 1;

            }

        }
    }
    render();
}

function render(){

}
