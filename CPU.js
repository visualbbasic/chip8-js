/**
 * Created with JetBrains WebStorm.
 * User: JasonOwnzBiatch
 * Date: 22/06/13
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */

var CPU = {
    //Logger flag for CPU instructions
    logging: false,
    //4kb memory for CHIP8 program
    memory: new Array(0xFFF),
    registers: {

        V: new Array(16),
        //I: Used to store memory address
        I: 0,
        //Program counter: used to store currently executing address
        PC: 0,
        //Stack pointer, It is used to point to the topmost level of the stack
        SP: 0,
        //Used to store the address that the interpreter should return to when finished with a subroutine
        stack: new Array(16),
        delayTimer: 0,
        soundTimer: 0

    },

    logOn: function(){
      this.logging = true;
    },

    logOff: function(){
      this.logging = false;
    },

    cycle: function(opcode){
        var vFirst = (opcode & 0xF000) >> 12;
        var vX = (opcode & 0x0F00) >> 8;
        var vY = (opcode & 0x00F0) >> 4;
        var vN = (opcode & 0x000F) >> 0;

        switch(vFirst){
            case 0x0:
                var curCode = opcode & 0x00FF;
                switch(curCode){
                    case 0x00E0:
                        log(this.logging, "0x00E0: Clear screen");
                        //clear screen code here
                        break;
                    case 0x00EE:
                        log(this.logging, "0x00E0: Return from a subroutine");
                        this.registers.PC = this.registers.stack[this.registers.SP];
                        this.registers.SP--;

                        //this.registers.PC += 2;
                        break;
                    default:
                        log(this.logging, "0xNNN: This instruction is skipped because it's only used for only machines");
                        break;
                }

                break;

            case 0x1:

                log(this.logging, "0x1nnn: Jump PC to location nnn");
                this.registers.PC += opcode & 0x0FFF;
                break;
            case 0x2:

                log(this.logging, "0x2nnn: Call subroutine at nn");
                this.registers.SP++;
                this.registers.stack[this.registers.SP] = this.registers.PC;
                this.registers.PC = opcode & 0x0FFF;
                break;
            case 0x3:

                log(this.logging, "0x3xnn: skip the next instruction if Vx = kk.");

                if(this.registers.V[vX] == (opcode & 0x0FF))
                    this.registers.PC += 2;

                break;

            case 0x4:
                log(this.logging, "0x4nn: Skip the next instruction if Vx != kk.")
                if(this.registers.V[vX] != (opcode & 0x0FF))
                    this.registers.PC += 2;
                break;

            case 0x5:
                log(this.logging, "0x5xy0: Skip next instruction if Vx = Vy.");
                if(this.registers.V[vX] == this.registers.V[vY])
                    this.registers.PC += 2;
                break;

            case 0x6:
                log(this.logging, "0x6xkk: Setting Vx = kk.");
                this.registers.V[vX] = opcode & 0x00FF;
                break;
            case 0x7:
                log(this.logging, "0x7xkk: Setting Vx = Vx + kk.");

                this.registers.V[vX] += (opcode & 0x00FF);

                if(this.registers.V[vX] > 255)
                    this.registers.V[vX] -= 256;

                break;

            case 0x8:

                switch(fourth){
                    case 0x0:
                        log(this.logging, "0x8xy0: Set Vx = Vy.");
                        this.registers.V[vX] = this.registers.V[vY];

                        break;
                    case 0x1:
                        log(this.logging, "0x8xy1: Set Vx = Vx OR Vy");
                        this.registers.V[vX] |=  this.registers.V[vY];
                        break;
                    case 0x2:
                        log(this.logging, "0x8xy2: Set Vx = Vx AND Vy");
                        this.registers.V[vX] &=  this.registers.V[vY];
                        break;
                    case 0x3:
                        log(this.logging, "0x8xy3: Set Vx = Vx XOR Vy");
                        this.registers.V[vX] ^=  this.registers.V[vY];
                        break;
                    case 0x4:
                        log(this.logging, "0x8xy4: Set Vx = Vx + Vy, V[F] = carry");
                        //Only the lowest 8 bit will be saved to V[x]
                        this.registers.V[vX] += this.registers.V[vY];

                        if(this.registers.V[vX] > 255){
                            this.registers.V[0xF] = 1;
                            this.registers.V[vX] -= 256;
                        } else {
                            this.registers.V[0xF] = 0;
                        }

                        if(this.registers.V[vX] > 255){
                            this.registers.V[vX] -= 256;
                        }

                        break;
                    case 0x5:
                        log(this.logging, "0x8xy5: If Vx > Vy, then VF is set to 1, otherwise 0. Then Vy is subtracted from Vx, and the results stored in Vx.");

                        //Only the lowest 8 bit will be saved to V[x]
                        if(this.registers.V[vX] > this.registers.V[vY])
                            this.registers.V[0xF] = 1;
                        else
                            this.registers.V[0xF] = 0;

                        this.registers.V[vX] -= this.registers.V[vY];

                        if(this.registers.V[vX] < 0)
                            this.registers.V[vX] += 256;

                        break;
                    case 0x6:
                        log(this.logging, "0x8xy6: If the least-significant bit of Vx is 1, then VF is set to 1, otherwise 0. Then Vx is divided by 2");

                        this.registers.V[0xF] = (this.registers.V[vX] & 0x000F > 0) ? 0x01 : 0x00;
                        this.registers.V[vX] >>= 1;

                        break;

                    case 0x7:
                        log(this.logging, "0x8xy7: Set Vx = Vy - Vx, set VF = NOT borrow");
                        //If Vy > Vx, then VF is set to 1, otherwise 0. Then Vx is subtracted from Vy, and the results stored in Vx.

                        if(this.registers.V[vY] > this.registers.V[vX])
                            this.registers.V[0xF] = 1;
                        else
                            this.registers.V[0xF] = 0;

                        this.registers.V[vX] = this.registers.V[vY] - this.registers.V[vX];

                        if(this.registers.V[vX] < 0)
                            this.registers.V[vX] -= 256;

                        break;

                    case 0xE:
                        log(this.logging, "0x8xyE: Set Vx = Vx SHL 1.");
                        //If the most-significant bit of Vx is 1, then VF is set to 1, otherwise to 0. Then Vx is multiplied by 2.

                        if((this.registers.V[vX] >> 7) > 0)
                            this.registers.V[0xF] = 0x01;
                        else
                            this.registers.V[0xF] = 0x00;

                        this.registers.V[vX] <<= 1;

                        if(this.registers.V[vX] > 255)
                            this.registers.V[vX] -= 256;

                        break;
                }

                break;
            case 0x9:
                log(this.logging, "0x9xy0: Skip next instruction if Vx != Vy.");
                // The values of Vx and Vy are compared, and if they are not equal, the program counter is increased by 2.
                if(this.registers.V[vX] != this.registers.V[vY])
                    this.registers.PC += 2;

                break;

            case 0xA:
                log(this.logging, "Annn: Set I = nnn.");

                this.registers.I = opcode & 0x0FFF;

                break;

            case 0xB:
                log(this.logging, "Bnnn: Jump to location nnn + V0.");
                //The program counter is set to nnn plus the value of V0.
                this.registers.PC += (opcode & 0x0FFF) + this.registers.V[0x0];
                break;
            case 0xC:
                log(this.logging, "Cxkk: Set Vx = random byte AND kk.");
                // The interpreter generates a random number from 0 to 255, which is then ANDed with the value kk.
                // The results are stored in Vx. See instruction 8xy2 for more information on AND.
                var randomNumber = Math.floor(Math.random() * 0xFF);
                this.registers.V[vX] = randomNumber & (opcode & 0x00FF);
                break;

            case 0xD:
                log(this.logging, "Dxyn: Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision.");
                break;

            case 0xE:
                var _0x00FF = opcode & 0x00FF;
                switch(_0x00FF){
                    case 0x009E:
                        log(this.logging, "Ex9E: Skip next instruction if key with the value of Vx is pressed.");
                        //Checks the keyboard, and if the key corresponding to the value of Vx is currently in the down position, PC is increased by 2.

                        break;
                    case 0x00A1:
                        log(this.logging, "ExA1: Skip next instruction if key with the value of Vx is not pressed.");
                        //Checks the keyboard, and if the key corresponding to the value of Vx is currently in the up position, PC is increased by 2.
                        break;
                }
                break;

            case 0xF:
                var _0x00FF = opcode & 0x00FF;
                switch(_0x00FF){
                    case 0x0007:
                        log(this.logging, "Fx07: Set Vx = delay timer value.");
                        //The value of DT is placed into Vx.
                        this.registers.V[vX] = this.registers.delayTimer;
                        break;

                    case 0x000A:
                        log(this.logging, "Fx0A: Wait for a key press, store the value of the key in Vx.");
                        //All execution stops until a key is pressed, then the value of that key is stored in Vx.

                        break;

                    case 0x0015:
                        log(this.logging, "Fx15: Set delay timer = Vx.");
                        //DT is set equal to the value of Vx.
                        this.registers.delayTimer = this.registers.V[vX];
                        break;


                    case 0x0018:
                        log(this.logging, "Fx18: Set sound timer = Vx.");
                        //ST is set equal to the value of Vx.
                        this.registers.soundTimer = this.registers.V[vX];
                        break;

                    case 0x001E:
                        log(this.logging, "Fx1E: Set I = I + Vx.");
                        this.registers.I += this.registers.V[vX];
                        break;

                    case 0x0029:
                        log(this.logging, "Fx29: Set I = location of sprite for digit Vx.");
                        //The value of I is set to the location for the hexadecimal sprite corresponding to the value of Vx.
                        // See section 2.4, Display, for more information on the Chip-8 hexadecimal font.
                        this.registers.I = this.registers.V[vX] * 5;
                        break;

                    case 0x0033:
                        log(this.logging, "Fx33: Store BCD representation of Vx in memory locations I, I+1, and I+2.");

                        break;

                    case 0x0055:
                        log(this.logging, "Fx55: Store registers V0 through Vx in memory starting at location I.");
                        for(var i = 0; i <= vX; i ++)
                            this.registers.memory[this.registers.I + i] = this.registers.V[i];
                        break;

                    case 0x0065:
                        log(this.logging, "Fx65: Read registers V0 through Vx from memory starting at location I.");
                        for(var i = 0; i <= vX; i++)
                            this.registers.V[i] = this.registers.memory[this.registers.I + i];
                        break;

                }
                break;
        }
    }

}



function emulateCycle(opcode) {
    //removing all follow up values after first binary by using and (&) operation
    var first = (opcode & 0xF000) >> 12;
    var second = (opcode & 0x0F00) >> 8;
    var third = (opcode & 0x00F0) >> 4;
    var fourth = (opcode & 0x000F) >> 0;


    switch(opcode & 0xF000){
        case 0x0000:
            switch(opcode & 0x000F){
                case 0x0000:
                    //clears screen
                    console.log("0x00E0: Cleaning screen!");
                    break;
                case 0x000E:

                    break;
                default:
                    console.log("0x0000 error: This is probably 0x0nnn which is not used anymore");
                    break;
            }
            break;
        case 0x1000:
            break;
    }

    switch (first) {
        case 0x0: // 0NNN, 00E0, 00EE

            console.log("Opcode is zero");
            break;
        case 0x1:   // 1NNN
            // Jumps to address NNN.
            pc = opcode & 0x0FFF;
            console.log("Opcode is one");
            break;
        case 0x2:   // 2NNN
            // Calls subroutine at NNN.
            stack[sp] = pc;
            ++sp;
            pc = opcode & 0x0FFF;
            console.log("Opcode is two");
            break;
        case 0x3:   // 3XNN
            // Skips the next instruction if VX equals NN.

            if (V[second] == (opcode & 0x00FF))
                pc += 4;
            else
                pc += 2;
            console.log("Opcode is three");
            break;
        case 0x4:   // 4XNN
            // Skips the next instruction if VX doesn't equal NN.

            if (V[second] != (opcode & 0x00FF))
                pc += 4;
            else
                pc += 2;
            console.log("Opcode is four");
            break;
        case 0x5:   // 5XY0
            // Skips the next instruction if VX equals VY.

            if (V[second] == V[third])
                pc += 4;
            else
                pc += 2;
            console.log("Opcode is five");
            break;
        case 0x6:   // 6XNN
            // Sets VX to NN.

            V[second] = opcode & 0x00FF;
            pc += 2;

            console.log("Opcode is 6");

            break;

        case 0x7: // 7XNN
            // Adds NN to VX.

            V[second] += opcode & 0x00FF;
            pc += 2;

            console.log("Opcode is seven");

            break;
        case 0x8:   // 8XY0 ~ 8XYE
            console.log("Opcode is eight");
            switch (fourth) {
                case 0x0:   // 8XY0
                    // Sets VX to the value of VY.
                    V[second] = V[third];
                    pc += 2;
                    break;
                case 0x1:   // 8XY1
                    // Sets VX to VX or VY.
                    V[second] |= V[third];
                    pc += 2;
                    break;
                case 0x2:   // 8XY2
                    // Sets VX to VX and VY.
                    V[second] &= V[third];
                    pc += 2;
                    break;
                case 0x3:   // 8XY3
                    // Sets VX to VX xor VY.
                    V[second] ^= V[third];
                    pc += 2;
                    break;
                case 0x4:   //8XY4
                    //Adds VY to VX. VF is set to 1 when there's a carry,
                    //and to 0 when there isn't.

                    if (V[third] > (0xFF - V[second]))
                        V[0xF] = 1;
                    else
                        V[0xF] = 0;

                    V[second] += V[third];
                    pc += 2;

                    break;

                case 0x5:   //8XY5
                    //VY is subtracted from VX.
                    // VF is set to 0 when there's a borrow,
                    // and 1 when there isn't.

                    if (V[third] > V[second])
                        V[0xF] = 1;
                    else
                        V[0xF] = 0;

                    V[second] -= V[third];
                    pc += 2;

                    break;

                case 0x6:   // 8XY6:
                    // Shifts VX right by one.
                    // VF is set to the value of the least
                    // significant bit of VX before the shift
                    V[0xF] = V[second] & 0x1;
                    V[second] >>= 1;
                    pc += 2;

                    break;

                case 0x7:   // 8XY7
                    // Sets VX to VY minus VX.
                    // VF is set to 0 when there's a borrow,
                    // and 1 when there isn't.

                    if (V[second] > V[third])
                        V[0xF] = 0;
                    else
                        V[0xF] = 1;

                    V[second] -= V[third] - V[second];
                    pc += 2;

                    break;

                case 0xE:   // 8XYE
                    // Shifts VX left by one.
                    // VF is set to the value of the most significant
                    // bit of VX before the shift.
                    V[0xF] = V[second] >> 7;
                    V[second] <<= 1;
                    pc += 2;

                    break;
            }
            break;
        case 0x9:   // 9XY0
            // Skips the next instruction if VX doesn't equal VY.
            console.log("Opcode is nine");
            if (V[second] != V[third])
                pc += 4;
            else
                pc += 2;

            break;

        case 0xA:   // ANNN
                    // Sets I to the address NNN.
            console.log("Opcode is A");
            REGISTER_I = opcode & 0x0FFF;
            pc += 2;
            break;

        case 0xB:   // BNNN
                    // Jumps to the address NNN plus V0.
            console.log("Opcode is B");
            pc = (opcode & 0x0FFF) + V[0];
            break;
        case 0xC:   // CXNN
                    // Sets VX to a random number and NN.
            console.log("Opcode is C");
            V[second] = (Math.random() % 0xFF) & (opcode & 0x00FF);
            pc += 2;
            break;

        case 0xD:   // DXYN
                    // Drawing a sprite
                    // Draws a sprite at coordinate (VX, VY)
                    // that has a width of 8 pixels and a height of N pixels.
                    // Each row of 8 pixels is read as bit-coded
                    // (with the most significant bit of each byte displayed on the left)
                    // starting from memory location I; I value doesn't change after the execution of this instruction.
                    // As described above,
                    // VF is set to 1 if any screen pixels are flipped from set to unset when the sprite is drawn,
                    // and to 0 if that doesn't happen.

            var x = V[second];
            var y = V[third];
            var width = 8;
            var height = opcode & 0x000F;
            //draw(x, y, width, height);


            pc += 2;
            break;

        case 0xE:   // EX9E, EXA1
            console.log("Opcode is E");
            switch(fourth){
                case 0xE:

                    if(key[V[second]] != 0)
                        pc += 4;
                    else
                        pc += 2;

                    break;
                case 0x1:

                    if(key[V[second]] == 0)
                        pc += 4;
                    else
                        pc += 2;

                    break;
                default:
                    console.log("Unknown opcode at EX9E");
            }
            break;

        case 0xF:
            console.log("Opcode is F");
            switch(fourth){
                case 0x7:   // FX07
                            // Sets VX to the value of the delay timer.
                    delay_timer = V[second];
                    pc += 2;
                    break;
                case 0xA:   // FX0A
                            // A key press is awaited, and then stored in VX.
                    var keyPressed = false;
                    for(var j = 0; j < key.length; j++){
                        if(key[j] != 0){
                            V[second] = j;
                            keyPressed = true;
                        }
                    }
                    console.log("key pressed");
                    if(!keyPressed)
                        return;

                    pc += 2;

                    break;
                case 0x5:   // FX15
                            // Sets the delay timer to VX.
                    delay_timer =  V[second];
                    pc += 2;

                    break;
                case 0x8:   // FX18
                            // Sets the sound timer to VX.

                    sound_timer = V[second];
                    pc += 2;
                    break;

                case 0xE:   //FX1E
                            //Adds VX to I.

                    I += V[second];
                    pc += 2;

                    break;
                case 0x9:   // FX29
                            // Sets I to the location of the sprite for the character in VX.
                            // Characters 0-F (in hexadecimal) are represented by a 4x5 font.

                    I = V[second] * 0x5;
                    pc += 2;

                    break;

                case 0x3:   // FX33
                            // Stores the Binary-coded decimal representation of VX,
                            // with the most significant of three digits at the address in I,
                            // the middle digit at I plus 1,
                            // and the least significant digit at I plus 2.
                            // (In other words, take the decimal representation of VX,
                            // place the hundreds digit in memory at location in I,
                            // the tens digit at location I+1, and the ones digit at location I+2.)

                    memory[REGISTER_I] = V[second] / 100;
                    memory[REGISTER_I + 1] = (V[second] / 10) % 10;
                    memory[REGISTER_I + 2] =  (V[second] % 100);
                    pc += 2;

                    break;

                case 0x5:
                    switch(third){

                        case 0x5:        // FX55
                                        // Stores V0 to VX in memory starting at address I.
                            for (var j =0; j < second; j++)
                                memory[I + j] = V[j];


                            // On the original interpreter, when the operation is done, I = I + X + 1.
                            I += second + 1;

                            pc += 2;
                            break;

                        case 0x6:   //FX65
                                    // Fills V0 to VX with values from memory starting at address I

                            for (var j =0; j < second; j++)
                                V[j] = memory[I + j];

                            // On the original interpreter, when the operation is done, I = I + X + 1.
                            I += second + 1;

                            pc += 2;

                            break;
                    }

                    break;

            }
            break;
        default:
            console.log("Invalid opcode!! lol");
    }

    console.log("first: " + first + " pc: " + pc);
}